# Backend CRUD API REST

En este documento veremos, paso a paso, la creación de un CRUD API REST.

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.


### Pre-requisitos 📋

Toda la práctica se realizó en la máquina virtual de LINUX con Ubuntu lts 20.04.

### Instalación 🔧

En primer ligarendremos que instalar Chrome (que lo haremos desde el buscador), Visual Studio Code y Postman.

```
sudo snap install --classic code
sudo snap install postman
```
En segundo lugar, procederemos a la instalación de Node JS y una utilidad que le ayuda a mantener el paquete actualizado.

```
sudo apt install npm
sudo npm i -g n
```
También tendremos que descargar el gestor de repositorios.

```
sudo apt install git
git config --global user.name (nombre de usuario)
git config --global user.email (email de usuario)
```

Junto con Express, Morgan y MongoDB (que la tendremos que instalar obligatoriamente en la carpeta donde estemos trabajando).

```
npm i -S express
npm i -S morgan
sudo apt install -y mongodb
npm i -S mongodb
npm i -S mongojs
```

## Ejecutando las pruebas ⚙️

Para poder tener un entorno de desarrollo ejecutandose, tendremos que crear un repositorio en Bitbucket y conectar con el repositorio remoto de la siguiente forma:

```
git remote -v
git remote add origin (link del repositorio)
```
Hay que recalcar que tendremos que crear una nueva carpeta para todos los proyectos en node.

Para subir los archivos al repositorio, tendremos que ejecutar tres instrucciones.

```
git add .
git commit -m "Nombre del archivo a subir"
git push
```

Una vez tengamos los archivos subidos, ejecutamos la siguiente línea de código, que nos dirá el cuerpo de nuestra solicitud

```
npm start
```

Ahora, cada vez que hagamos una orden GET, POST, PUT, DELETE en Postman, tendremos constancia en nuestra terminal.

En una terminal nueva, abriremos el gestor de base de datos y podremos ver todas las subidas que hayamos realizado desde Postman.

### Analice las pruebas end-to-end 🔩

Si realizamos un POST en Postman, también lo tendremos en nuesta base de datos local.

### Y las pruebas de estilo de codificación ⌨️

_Explica qué verifican estas pruebas y por qué_

## Despliegue 📦

_Agrega notas adicionales sobre cómo hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Ana Martínez Hernández** - *Practica 1* - [Ana](https://bitbucket.org/thecandy111/api-rest/src/master/)


## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.
