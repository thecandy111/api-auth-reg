'use strict'

// En la P03, de página 30 para abajo está todo.

const port = process.env.PORT || 3000

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();

var db = mongojs("SD"); //Enlazamos con la BD "SD"
var id = mongojs.ObjectID; //Función para convertir un id textual en un objetcID.

//middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}))
app.use(express.json());

//Trigger para múltiples colecciones de usuarios:
app.param("usuario", (req, res, next, usuario) => {
    console.log('param /api/:usuario');
    console.log('usuario: ', usuario);

    req.collection = db.collection(usuario);
    return next();
});

//Routes:
app.get('/api', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, usuarios) => {
        if (err) return next(err);
        res.json(usuarios);
    });
});

//Hay que tener en cuenta de que donde pone colección tiene que ser
//un usuario (user) y el ID ya no es de un producto, sino del propio usuario.
app.get('/api/:usuario', (req, res, next) => {
    req.collection.find((err,usuario) => {
        if (err) return next (err);
        res.json(usuario);
    });
});

app.get('/api/:usuario/:id', (req,res,next) => {
    req.collection.findOne({ _id: id(req.params.id)}, (err, elemento) => {
        if (err) return next (err);
        res.json(elemento);
    });
});

app.post('/api/:usuario', (req, res, next) => {
    const elemento = req.body;

    if(!elemento.nombre){
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    } else {
        req.collection.save(elemento, (err,usuarioGuardado) => {
            if (err) return next (err);
            res.json(usuarioGuardado);
        });
    }
});

app.put('/api/:usuario/:id', (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    req.collection.update({ _id: id(elementoId)},
            {$set: elementoNuevo}, {safe: true, multi: false}, (err,elementoModif) => {
        if (err) return next(err);
        res.json(elementoModif);
    });
});

app.delete('/api/:usuario/:id', (req, res, next) => {
    let elementoId = req.params.id;

    req.collection.remove({ _id: id(elementoId)}, (err,resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});


//Inicializamos la aplicación
app.listen(port, () => {
    console.log(`API REST ejecutandose en http://localhost:${port}/api/:usuario/:id`);
});